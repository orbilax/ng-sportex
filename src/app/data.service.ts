import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {News, Page} from './payload';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url = 'http://localhost:8080';

  constructor(private http: HttpClient) {
  }

  getNews(page = 0, size = 10): Observable<Page<News>> {
    return this.http
      .get<Page<News>>(
        `${this.url}/news?page=${page}&size=${size}&sort=createdAt,desc`
      );
  }

  getNewsItem(id: number): Observable<News> {
    return this.http
      .get<News>(`${this.url}/news/${id}`);
  }

  getPhotoUrl(cont: string): string {
    return this.url + cont;
  }

}
