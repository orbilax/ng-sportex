import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsCardListComponent} from './news-card-list/news-card-list.component';

const routes: Routes = [
  {path: 'news', component: NewsCardListComponent},
  {path: '', redirectTo: 'news', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
