import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../data.service';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {News} from '../payload';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, mergeMap, scan, tap, throttleTime} from 'rxjs/operators';

@Component({
  selector: 'app-news-card-list',
  templateUrl: './news-card-list.component.html',
  styleUrls: ['./news-card-list.component.scss']
})
export class NewsCardListComponent implements OnInit {
  @ViewChild(CdkVirtualScrollViewport)
  viewport: CdkVirtualScrollViewport;

  theEnd = false;

  offset = new BehaviorSubject(null);

  lastPage = 0;

  infiniteNews: Observable<News[]>;


  constructor(private dataService: DataService) {
    const batchMap = this.offset.pipe(
      throttleTime(500),
      mergeMap(() => this.getBatch()),
      scan((acc, batch) => {
        return [...acc, ...batch];
      }, [])
    );

    this.infiniteNews = batchMap.pipe(
      map(value => {
        return Object.values(value);
      })
    );
  }

  ngOnInit() {
    // this.getNextBatch(null);
  }

  nextBatch(e, offset) {
    if (this.theEnd) {
      return;
    }

    const end = this.viewport.getRenderedRange().end;
    const total = this.viewport.getDataLength();

    if (end === total) {
      this.offset.next(offset);
    }
  }

  trackByIdx(i) {
    return i;
  }


  getBatch() {
    return this.dataService
      .getNews(this.lastPage++)
      .pipe(
        tap(page => this.theEnd = page.last),
        map(page => {
          return page.content;
        })
      );
  }
}
