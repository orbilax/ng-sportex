  export interface Sort {
    unsorted: boolean;
    sorted: boolean;
    empty: boolean;
  }

  export interface Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
  }

  export interface Page<T> {
    content: T[];
    pageable: Pageable;
    totalPages: number;
    totalElements: number;
    last: boolean;
    size: number;
    number: number;
    sort: Sort;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
  }

  export interface Photo {
    id: number;
    photoName: string;
    photoUrl: string; // todo check error on server side
  }

  export interface News {
    id: number;
    title: string;
    header: Photo;
    info: string;
    photos: Photo[];
    source: string;
    createdAt: Date;
  }
